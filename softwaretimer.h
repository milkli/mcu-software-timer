
/************************************************************************
 * @FileName           : softwaretimer.h
 * @Description        : 软件虚拟定时器头文件
 * @Author             : MilkLi
 * @Date               : 2021-07-30 11:08:29
 * @LastEditors        : MilkLi
 * @LastEditTime       : 2022-02-07 16:51:19
 * @MaintenanceLogbook : 
/******************************************************************************/

#ifndef __SOFTWARETIMER_H__
#define __SOFTWARETIMER_H__
/**
 * @brief 	这里定义了一些本程序中常用的数据类型。
 * 		一般来说在C51中short、unsigned short、int、
 * 		unsigned int都是占用2个字节；long、
 * 		SWT_uint32_t型占用4个字节，并没有占用8个字节
 * 		的数据类型。
 * 			然而这在STM32等ARM系列的单片机中并不是这样。
 * 		在这些单片机中short、unsigned short型是占用2个
 * 		字节，而int、unsigned int、long是占用4个字节。
 * 		在STM32中还存在一种long long的数据类型占用8个字节。
 * 			综上所述，这里选择了这两个平台数据类型占用
 * 		的字节数相近的数据类型作为本程序的数据类型。
 * 
 */
typedef unsigned char SWT_uint8_t;
typedef char SWT_int8_t;
typedef unsigned short SWT_uint16_t;
typedef short SWT_int16_t;
typedef unsigned long SWT_uint32_t;
typedef long SWT_int32_t;

/**
 * @brief 软件定时器ID枚举类型
 * 		可以在此处添加变量，标记使用的定时器内容。
 * 		增加代码的可阅读性。
 * 
 */
enum SWID
{
	SW_MAX
};

#ifndef NULL
#define NULL ((void *)0)
#endif
#define FOREVER -1	//无限循环次数

#define SOFTWARE_TIMER_NUM 5		//软件定时器总数量
									//需要按照项目的需求更改。
									//建议调试结束之后，用到多少软件定时器就定义多少。
									//显而易见的是这个数值越大，系统整体的效率越低。

typedef void (*timeout_handler)();	//软件定时器回调函数
/* 软件定时器结构体声明 */
typedef struct
{
	SWT_uint8_t id;		  //软件定时器ID
	SWT_uint32_t count;	  //计数值
	SWT_uint32_t expire;	  //重装载值
	timeout_handler callback; //回调函数
	SWT_uint8_t isEnable;	  //定时器使能 0:disable 1:enable 
	SWT_int16_t repeat;				  //定时器循环次数 -1:forever 
	SWT_uint8_t run;		  //回调函数运行标志 0:不运行，1:运行
} SoftwareTimerStruct;
/* 软件定时器结构体变量声明 */
extern SoftwareTimerStruct SoftwareTimerList[SOFTWARE_TIMER_NUM];
/* 函数声明 */
void SoftwareTimer_Tick(void);
void SoftwareTimer_Init(SWT_uint8_t id, SWT_uint32_t count, SWT_uint32_t expire, timeout_handler callback, SWT_int16_t repeat);
void SoftwareTimer_Deinit(SWT_uint8_t id);
void SoftwareTimer_Enable(SWT_uint8_t id);
void SoftwareTimer_Disable(SWT_uint8_t id);
SWT_uint8_t SoftwareTimer_Get_Enable(SWT_uint8_t id);
void SoftwareTimer_Set_Enable(SWT_uint8_t id, SWT_uint8_t able);
void SoftwareTimer_Set_Expire(SWT_uint8_t id, SWT_uint32_t expire);
SWT_uint32_t SoftwareTimer_Get_Expire(SWT_uint8_t id);
void SoftwareTimer_Loop(void);
void SoftwareTimer_Reload_Count(SWT_uint8_t id);
void SoftwareTimer_Set_Count(SWT_uint8_t id, SWT_uint32_t count);
SWT_uint32_t SoftwareTimer_Get_Count(SWT_uint8_t id);
void SoftwareTimer_Set_Repeat(SWT_uint8_t id, SWT_int16_t repeat);
SWT_int16_t SoftwareTimer_Get_Repeat(SWT_uint8_t id);
#endif
